import "./IntervalVector.css";
import { INTERVAL_VEC, INTERVAL_VEC_TH } from "../global/constants";
import { useSelector } from "react-redux";

function IntervalVector() {
  const intervalVectorState = useSelector((state) => state.intervalVector)[0];
  const printIntervalVector = () => {
    let drawVector = "<table> <tr>";
    for (let i = 0; i < INTERVAL_VEC; i++) {
      drawVector += "<th class='cellV_th'>" + INTERVAL_VEC_TH[i] + "</th>";
    }
    drawVector += "</tr><tr>";
    for (let i = 0; i < INTERVAL_VEC; i++) {
      drawVector += "<td class='cellV_td'>" + intervalVectorState[i] + "</td>";
    }
    drawVector += "</tr></table>";
    return drawVector;
  };

  return (
    <div>
      <div className="titleVector">Vector Interválico</div>
      <div dangerouslySetInnerHTML={{ __html: printIntervalVector() }}></div>
    </div>
  );
}

export default IntervalVector;
