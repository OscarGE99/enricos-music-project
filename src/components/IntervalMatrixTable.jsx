import React, { useState, useEffect } from "react";
import "./IntervalMatrixTable.css";
import { ROW, COLUMN, reverse } from "../global/constants";
import notesList from "../global/notesList";
import { useSelector, useDispatch } from "react-redux";
import { setIntervalForm } from "../features/intervalFormSlice";
import { setPartitions } from "../features/partitionsSlice";

function IntervalMatrixTable() {
  const intervalMatrixState = useSelector((state) => state.intervalMatrix)[0];
  const preIntervalMatrixState = useSelector((state) => state.preIntervalMatrix)[0];
  const intervalFormState = useSelector((state) => state.intervalForm)[0];
  const partitionsState = useSelector((state) => state.partitions)[0];
  const dispatch = useDispatch();

  const printIntervalMatrix = () => {
    let drawMatrix = "<table>";
    let className = "cellM notas";
    for (let i = 0; i <= ROW; i++) {
      drawMatrix += "<tr>";
      for (let j = 0; j <= COLUMN; j++) {
        if (i === 0 || j === 0 || i === ROW || j === COLUMN || i + j === 12) {
          className += " border";
        }
        if (
          notesList.map((note) => note.note).includes(intervalMatrixState[i][j])
        ) {
          className += " note";
        }
        if (intervalMatrixState[i][j].includes("-")) {
          className += " redNote";
        }
        drawMatrix += `<th class="${className}"> ${intervalMatrixState[i][j]} </th>`;
        className = "cellM";
      }
      drawMatrix += "</tr>";
    }
    drawMatrix += "</table>";
    return drawMatrix;
  };
  const printIntervalMatrixTurned = () => {
    let drawMatrix = "<table>";
    let className = "cellM notas";
    for (let i = 0; i <= ROW; i++) {
      drawMatrix += "<tr>";
      for (let j = 0; j <= COLUMN; j++) {
        if (i === 0 || j === 0 || i === ROW || j === COLUMN || i === j) {
          className += " border";
        }
        if (
          notesList.map((note) => note.note).includes(intervalMatrixState[i][COLUMN-j])
        ) {
          className += " note";
        }
        if (intervalMatrixState[i][COLUMN - j].includes("-")) {
          className += " redNote";
        }
        drawMatrix += `<th class="${className}"> ${intervalMatrixState[i][COLUMN-j]} </th>`;
        className = "cellM";
      }
      drawMatrix += "</tr>";
    }
    drawMatrix += "</table>";
    return drawMatrix;
  };

  const printPreIntervalMatrix = () => {
    let drawMatrix = "<table>";
    let className = "cellM numeros";
    for (let i = 0; i <= ROW; i++) {
      drawMatrix += "<tr>";
      for (let j = 0; j <= COLUMN + 1; j++) {
        if (i === 0 && j === COLUMN + 1) {
          className = "";
        }
        if ((i !== 0 && j === COLUMN + 1) || (i === 0 && j < COLUMN + 1)) {
          className += " note border";
        }
        if (i !== 0 && j === COLUMN) {
          className += " border";
        }
        if (j + i === COLUMN && i !== 0 && j !== COLUMN) {
          className += " note border";
        }
        if (i + j < 12 && i !== 0) {
          className += " redNote";
        }
        drawMatrix += `<th class="${className}"> ${preIntervalMatrixState[i][j]} </th>`;
        className = "cellM";
      }
      drawMatrix += "</tr>";
    }
    drawMatrix += "</table>";
    return drawMatrix;
  };

  const printPreIntervalMatrixTurned = () => {
    let drawMatrix = "<table>";
    let className = "cellM numeros";
    for (let i = 0; i <= ROW; i++) {
      drawMatrix += "<tr>";
      for (let j = 0; j <= COLUMN + 1; j++) {
        if (i === 0 && j === 0) {
          className = "";
        }
        if ((i !== 0 && j === 0) || (i === 0 && j >= 1)) {
          className += " note border";
        }
        if (j === 1 && i !== 0) {
          className += " border";
        }
        if (j - i === 1 && i !== 0 && j !== 1) {
          className += " note border";
        }
        if (i < j && j - i !== 1 && i >= 1) {
          className += " redNote";
        }
        drawMatrix += `<th class="${className}"> ${preIntervalMatrixState[i][(COLUMN+1)-j]} </th>`;
        className = "cellM";
      }
      drawMatrix += "</tr>";
    }
    drawMatrix += "</table>";
    return drawMatrix;
  };

  useEffect(() => {
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  });

  const [turnBtn, setTurnBtn] = useState("call_made");
  const [printlMatrix, setPrintlMatrix] = useState("notes");

  const handleClick = (e) => {
    if (
      e.target.className.includes("turn_btn") ||
      e.target.className.includes("material-symbols-outlined tb")
    ) {
      if (turnBtn === "call_made" && reverse.isReverse === false) {
        setTurnBtn("north_west");
        dispatch(setIntervalForm(Object.values(intervalFormState).reverse()));
        dispatch(setPartitions(Object.values(partitionsState).reverse()));
        reverse.isReverse = true;
      }
      if (turnBtn === "north_west" && reverse.isReverse === true) {
        setTurnBtn("call_made");
        dispatch(setIntervalForm(Object.values(intervalFormState).reverse()));
        dispatch(setPartitions(Object.values(partitionsState).reverse()));
        reverse.isReverse = false;
      } 
    }
  }
  var matrixToPrint;
  if (printlMatrix === "notes" && turnBtn === "call_made") {
    matrixToPrint = printIntervalMatrix();
  }
  if (printlMatrix === "notes" && turnBtn === "north_west") {
    matrixToPrint = printIntervalMatrixTurned();
  }
  if (printlMatrix === "numbers" && turnBtn === "call_made") {
    matrixToPrint = printPreIntervalMatrix();
  }
  if (printlMatrix === "numbers" && turnBtn === "north_west") {
    matrixToPrint = printPreIntervalMatrixTurned();
  }
  
  const onChangeValue = (e) => {
    if (e.target.value === "notes") {
      setPrintlMatrix("notes");
      matrixToPrint = printIntervalMatrix();
    } else if (e.target.value === "numbers") {
      setPrintlMatrix("numbers");
      matrixToPrint = printPreIntervalMatrix();
    }
  };
  return (
    <div>
      <div className="titleMatriz">Matriz Interválica</div>
      <div className="toggle-switch-m" onChange={onChangeValue}>
        <input
          type="radio"
          id="radio-one-m"
          name="switch-one-m"
          value="notes"
          defaultChecked
        />
        <label htmlFor="radio-one-m">Intervalos</label>
        <input
          type="radio"
          id="radio-two-m"
          name="switch-one-m"
          value="numbers"
        />
        <label htmlFor="radio-two-m">Dígitos</label>
      </div>
      <button className="turn_btn">
        <span className="material-symbols-outlined tb">{turnBtn}</span>
      </button>
      <div dangerouslySetInnerHTML={{ __html: matrixToPrint }}></div>
    </div>
  );
}

export default IntervalMatrixTable;
