import React, { useState, useEffect } from "react";
import "./Piano.css";
import Key from "./Key";
import BinArray from "./BinArray";
import notesList from "../global/notesList";
import { VALID_KEYS, PRESSEDKEY, reverse} from "../global/constants";
import {useDispatch } from "react-redux";
import { setIntervalMatrix } from "../features/intervalMatrixSlice";
import { setIntervalForm } from "../features/intervalFormSlice";
import { setIntervalVector } from "../features/intervalVectorSlice";
import { setPreIntervalMatrix } from "../features/preIntervalMatrixSlice";
import { setPartitions } from "../features/partitionsSlice";
import { setCombination } from "../features/combinationSlice";
const { interval_matrix, intervalic_formula, intervalic_vector } = require("music-interval-calc");

function Piano() {
  const dispatch = useDispatch();
  const [rendering, setRendering] = useState(0);
  const [muteBtn, setMuteBtn] = useState("volume_off");

  const getIntervalCalc = () => {
    setRendering(rendering + 1);
    const input = notesList.map((note) => note.isplaying).reverse();

    const resMatrix = interval_matrix(input);
    dispatch(setIntervalMatrix(resMatrix.intervalMatrixSort));
    dispatch(setPreIntervalMatrix(resMatrix.preIntervalMatrixSort));
    
    const resForm = intervalic_formula(input);
    resForm.squaredVector.reverse();
    if (reverse.isReverse===false) {
      dispatch(setIntervalForm(resForm.intervalForm));
      dispatch(setPartitions(resForm.partition));
    }
    else{
      dispatch(setIntervalForm(resForm.intervalForm.reverse()));
      dispatch(setPartitions(resForm.partition.reverse()));
    }
    dispatch(setCombination(resForm.combination));
    notesList.map((note) => (note.squared = resForm.squaredVector[note.position]));

    const resVector = intervalic_vector(input);
    dispatch(setIntervalVector(resVector));
  };
  
  const playNote = (note) => {
    if (note && muteBtn === "volume_up") {
      const noteAudio = new Audio(document.getElementById(note).src);
      noteAudio.play();
    } 
  }  

  useEffect(() => {
    document.addEventListener("keydown", handleKeyDown);
    document.addEventListener("keyup", handleKeyUp);
    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
      document.removeEventListener("keyup", handleKeyUp);
      document.removeEventListener("click", handleClick);
    };
  });

  const handleClick = (e) => {
    if (
      e.target.className.includes("key") ||
      e.target.className.includes("binArray")
    ) {
      if (e.repeat) {
        return;
      }
      const position = e.target.firstChild.id;
      if (notesList[position].isplaying === 0) {
        notesList[position].isplaying = 1;
        playNote(notesList[position].forsaund);
      } else if (notesList[position].isplaying === 1) {
        notesList[position].isplaying = 0;
      }
      getIntervalCalc();
    } else if (e.target.className.includes("reset_btn")) {
      for (let i in notesList) {
        notesList[i].isplaying = 0;
      }
      getIntervalCalc();
    } else if (
      e.target.className.includes("volume_btn") ||
      (e.target.className.includes("material-symbols-outlined") &&
        !e.target.className.includes("material-symbols-outlined tb"))
    ) {
      if (muteBtn === "volume_off") {
        setMuteBtn("volume_up");
      } else {
        setMuteBtn("volume_off");
      }
    }
  }; 

  const handleKeyDown = (e) => {
    if (e.repeat) {
      return;
    }
    const key = e.key;
    if (VALID_KEYS.includes(key)) {
      if (notesList[PRESSEDKEY.get(key)].isplaying === 0) {
        notesList[PRESSEDKEY.get(key)].isplaying = 1;
        playNote(notesList[PRESSEDKEY.get(key)].forsaund);
        getIntervalCalc();
      }
    }
  };

  const handleKeyUp = (e) =>{
    const key = e.key;
    if (VALID_KEYS.includes(key)) {
      if (notesList[PRESSEDKEY.get(key)].isplaying === 1) {
        notesList[PRESSEDKEY.get(key)].isplaying = 0;
        getIntervalCalc();
      }
    }
  }

  const keys = notesList.map((note) => {
    return (
      <Key
        key={note.position}
        position={note.position}
        note={note.note}
        color={note.color}
        isplaying={note.isplaying}
        noteDodecagon={note.noteDodecagon}
        colorDodecagon={note.colorDodecagon}
      />
    );
  });
  const binArray = notesList.reverse().map((note) => {
    return (
      <BinArray
        key={note.position}
        position={note.position}
        note={note.note}
        color={note.color}
        isplaying={note.isplaying}
        squaredVector={note.squared}
        noteDodecagon={note.noteDodecagon}
        colorDodecagon={note.colorDodecagon}
      />
    );
  });
  notesList.reverse();

  const audioFiles = notesList.map(note => {
    return (
      <audio
        key={note.position}
        id={note.forsaund}
        src={`../../sounds/${note.forsaund}.mp3`}
      />
    );
  });  
  const [printInput, setPrintInput] = useState("piano");
  const onChangeValuePiano = (e) => {
    if (e.target.value === "piano") {
      setPrintInput("piano");
    } else if (e.target.value === "vector") {
      setPrintInput("vector");
    }
  };
  var inputToPrint;
  var inputClassName;
  if (printInput === "piano") {
    inputToPrint = keys;
    inputClassName="piano";
  }
  if (printInput === "vector") {
    inputToPrint = binArray;
    inputClassName = "vector";
  }
  return (
    <div>
      <button className="volume_btn">
        <span className="material-symbols-outlined">{muteBtn}</span>
      </button>
      <button className="reset_btn">Limpiar</button>
      <div className="toggle-switch" onChange={onChangeValuePiano}>
        <input
          type="radio"
          id="radio-one"
          name="switch-one"
          value="piano"
          defaultChecked
        />
        <label htmlFor="radio-one">Piano</label>
        <input type="radio" id="radio-two" name="switch-one" value="vector" />
        <label htmlFor="radio-two">Vector binario</label>
      </div>
      <div className={inputClassName}>{inputToPrint}</div>
      <div>{audioFiles}</div>
    </div>
  );
}

export default Piano;
