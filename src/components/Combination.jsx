import "./Combination.css";
import { useSelector } from "react-redux";

function Combination() {
  const combinationState = useSelector((state) => state.combination)[0];

  const printCombination = () => {
    let drawCombination = "<table> <tr>";
    drawCombination += "<th class='cellC'>" + combinationState + "</th>";
    drawCombination += "</tr></table>";
    return drawCombination;
  };

  return (
    <div>
      <div className="titleCombination">Combinación</div>
      <div dangerouslySetInnerHTML={{ __html: printCombination() }}></div>
    </div>
  );
}

export default Combination;
