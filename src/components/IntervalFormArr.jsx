import "./IntervalFormArr.css";
import { FORMULA_ARR } from "../global/constants";
import { useSelector } from "react-redux";

function IntervalFormArr() {
  const intervalFormState = useSelector((state) => state.intervalForm)[0];
  
  const printIntervalFormArr = () => {
    let drawFormula = "<table> <tr>";
    for (let i = 0; i < FORMULA_ARR; i++) {
      drawFormula += "<th class='cellF'>" + intervalFormState[i] + "</th>";
    }
    drawFormula += "</tr></table>";
    return drawFormula;
  };

  return (
    <div>
      <div className="titleForm">Forma Interválica</div>
      <div dangerouslySetInnerHTML={{ __html: printIntervalFormArr() }}></div>
    </div>
  );
}

export default IntervalFormArr;
