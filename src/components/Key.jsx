import "./Key.css";

function Key(props) {
  let keyClassName = "key";
  let colorLinea;
  props.color === "black" ? keyClassName += " sharp" : keyClassName+= "";
  if (props.isplaying === 1) {
    keyClassName += " pressed";
    colorLinea = props.colorDodecagon;
  }
  else{
    keyClassName += "";
    colorLinea = "";
  }

  let key;
  if (props.color === "black") {
    key = (
      <div className={keyClassName}>
        <div className="key-txt-sharp" id={props.position}>
          {props.note}
          <hr className="noteLinea" color={colorLinea} />
          <div className="subNote">{props.noteDodecagon}</div>
        </div>
      </div>
    );
  } else {
    key = (
      <div className={keyClassName}>
        <div className="key-txt" id={props.position}>
          {props.note}
          <hr className="noteLinea" color={colorLinea} />
          <div className="subNote">{props.noteDodecagon}</div>
        </div>
      </div>
    );
  }

  return key;
}

export default Key;
