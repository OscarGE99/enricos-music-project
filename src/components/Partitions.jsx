import "./Partitions.css";
import { FORMULA_ARR } from "../global/constants";
import { useSelector } from "react-redux";

function Partitions() {
  const partitionsState = useSelector((state) => state.partitions)[0];

  const printPartitions = () => {
    let drawPartitions = "<table> <tr>";
    for (let i = 0; i < FORMULA_ARR; i++) {
      drawPartitions += "<th class='cellP'>" + partitionsState[i] + "</th>";
    }
    drawPartitions += "</tr></table>";
    return drawPartitions;
  };

  return (
    <div>
      <div className="titlePartitions">Partición</div>
      <div dangerouslySetInnerHTML={{ __html: printPartitions() }}></div>
    </div>
  );
}

export default Partitions;
