import "./Dodecagon.css";
import { useRef, useEffect, } from "react";
import {
  INNERDODECAGON,
  OUTERDODECAGON,
  NODECOLORS,
  ANGLE,
  NODERADIUS,
} from "../global/constants";
import notesList from "../global/notesList";
import { useSelector } from "react-redux";

function Dodecagon() {
  const combinationState = useSelector((state) => state.combination)[0];
  const ref = useRef(null);

  useEffect(() => {

    function init() {
      ctx.clearRect(0, 0, canvas.width, canvas.height);
      drawText(171, 179, 128, INNERDODECAGON, "bold", 15);
      drawDodecagon(175, 175, 110, 3);
      drawNodes(175, 175, 110, 3);
      drawText(169, 181, 165, OUTERDODECAGON, "normal", 18);
      drawDodecagon(175, 175, 150, 1);
      drawArcs();
    }

    function drawDodecagon(centerX, centerY, radio, lineWidth) {
      ctx.lineWidth = lineWidth;
      ctx.beginPath();
      ctx.strokeStyle = "white";
      for (var i = 0; i < 12; i++) {
        let x = centerX + radio * Math.cos(ANGLE * i);
        let y = centerY + radio * Math.sin(ANGLE * i);
        ctx.lineTo(x, y);
      }
      ctx.closePath();
      ctx.stroke();
    }

    function drawNodes(centerX, centerY, radio) {
      for (var i = 0; i < 12; i++) {
        let x = Math.round(centerX + radio * Math.cos(ANGLE * i));
        let y = Math.round(centerY + radio * Math.sin(ANGLE * i));
        let color = NODECOLORS[i];
        let note = OUTERDODECAGON[i];
        ctx.beginPath();
        ctx.fillStyle = color;
        ctx.moveTo(x, y);
        ctx.arc(x, y, NODERADIUS, 0, 2 * Math.PI);
        ctx.fill();
        nodes.push({ x, y, color, note });
      }
    }

    function drawText(centerX, centerY, radio, textDodecagon, weight, size) {
      ctx.font = `${weight} ${size}px ELEGANT`;
      ctx.fillStyle = "white";
      for (var i = 0; i < 12; i++) {
        let x = centerX + radio * Math.cos(ANGLE * i);
        let y = centerY + radio * Math.sin(ANGLE * i);
        ctx.fillText(textDodecagon[i], x, y);
      }
    }

    function drawArcs() {
      for (let i = 0; i < arcs.length; i++) {
        ctx.beginPath();
        let grad = ctx.createLinearGradient(
          arcs[i].startNode.x,
          arcs[i].startNode.y,
          arcs[i].endNode.x,
          arcs[i].endNode.y
        );
        grad.addColorStop(0, arcs[i].startNode.color);
        grad.addColorStop(1, arcs[i].endNode.color);
        ctx.strokeStyle = grad;
        ctx.lineWidth = 5;
        ctx.moveTo(arcs[i].startNode.x, arcs[i].startNode.y);
        ctx.lineTo(arcs[i].endNode.x, arcs[i].endNode.y);
        ctx.stroke();
      }
    }

    const canvas = ref.current;
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    let nodes = [];
    let arcs = [];
    let startNodes = [];
    let endNodes = [];
    init();

    for (let i = 0; i < nodes.length; i++) {
      for (let j = 0; j < notesList.length; j++) {
        if (nodes[i].note === notesList[j].note && notesList[j].isplaying === 1) {
          ctx.beginPath();
          ctx.lineWidth = 5;
          ctx.strokeStyle = "#A8E0DC";
          ctx.moveTo(nodes[i].x + NODERADIUS, nodes[i].y);
          ctx.arc(nodes[i].x, nodes[i].y, NODERADIUS + 2, 0, 2 * Math.PI);
          ctx.stroke();
          startNodes.push(nodes[i]);
          endNodes.push(nodes[i]);
        }
      }
    }

    for (let i = 0; i < startNodes.length; i++) {
      for (let j = 0; j < endNodes.length; j++) {
        if (startNodes[i].note !== endNodes[j].note) {
          let startNode = startNodes[i];
          let endNode = endNodes[j];
          arcs.push({ startNode, endNode });
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          init();
        }
      }
    }
  }, [combinationState]);
   
  return (
    <div>
      <div className="titleDodecagon">Dodecágono Orbital </div>
      <canvas ref={ref} id="canvas" width="358" height="350" />
    </div>
  );
}

export default Dodecagon;
