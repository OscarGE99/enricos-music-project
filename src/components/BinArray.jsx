import "./BinArray.css";

function BinArray(props) {
  let binArrayClassName = "binArray";
  let headClassName, colorLinea;
  props.color === "black"
    ? (headClassName = "headBlack binArray-txt-sharp")
    : (headClassName = "headWhite binArray-txt");
  if (props.isplaying === 1) {
    binArrayClassName += " pressed";
    colorLinea = props.colorDodecagon;
  } else {
    binArrayClassName += "";
    colorLinea = "";
  }

  let binArray;
  binArray = (
    <div>
      <div className={headClassName}>
        {props.note}
        <hr className="noteLinea" />
        <div className="subNote">{props.noteDodecagon}</div>
      </div>
      <div className={binArrayClassName}>
        <div className="binary-txt" id={props.position}>
          {props.isplaying}
          <hr className="noteLineaBin" color={colorLinea} />
        </div>
      </div>
      <div className="squared-txt">{props.squaredVector}</div>
    </div>
  );
  

  return binArray;
}

export default BinArray;
