import "./App.css";
import Piano from "../components/Piano";
import IntervalMatrixTable from "../components/IntervalMatrixTable";
import IntervalFormArr from "../components/IntervalFormArr";
import IntervalVector from "../components/IntervalVector";
import Partitions from "../components/Partitions";
import Combination from "../components/Combination";
import Dodecagon from "../components/Dodecagon";

function App() {
 
  return (
    <div className="App">
      <header className="App-header">
        <div className="container">
          <div className="sectionPiano">
            <Piano />
            <br />
            <Dodecagon />
          </div>
          <div className="sectionInfo">
            <IntervalMatrixTable />
            <br />
            <Combination />
            <br />
            <Partitions />
            <br />
            <IntervalFormArr />
            <br />
            <IntervalVector />
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
