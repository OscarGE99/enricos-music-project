import { configureStore } from "@reduxjs/toolkit";
import intervalMatrixReducer from "../features/intervalMatrixSlice";
import intervalFormReducer from "../features/intervalFormSlice";
import intervalVectorReducer from "../features/intervalVectorSlice";
import preIntervalMatrixReducer from "../features/preIntervalMatrixSlice";
import partitionsReducer from "../features/partitionsSlice";
import combinationReducer from "../features/combinationSlice";


export const store = configureStore({
  reducer: {
    intervalMatrix: intervalMatrixReducer,
    intervalForm: intervalFormReducer,
    intervalVector: intervalVectorReducer,
    preIntervalMatrix: preIntervalMatrixReducer,
    partitions: partitionsReducer,
    combination: combinationReducer,
  },
});

