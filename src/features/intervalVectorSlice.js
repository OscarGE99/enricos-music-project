import { createSlice } from "@reduxjs/toolkit";
import { INTERVAL_VEC } from "../global/constants";

const initialState = () => {
  var intervalVector = new Array(INTERVAL_VEC).fill(0);
  return [intervalVector];
};

export const intervalVectorSlice = createSlice({
  name: "intervalVector",
  initialState,
  reducers: {
    setIntervalVector: (state, action) => {
      state.pop();
      state.push(action.payload);
    },
  },
});

export const { setIntervalVector } = intervalVectorSlice.actions;
export default intervalVectorSlice.reducer;
