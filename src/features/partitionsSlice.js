import { createSlice } from "@reduxjs/toolkit";
import { FORMULA_ARR } from "../global/constants";

const initialState = () => {
  var partitions = new Array(FORMULA_ARR).fill("");
  partitions[FORMULA_ARR - 1] = "12";
  return [partitions];
};

export const partitionsSlice = createSlice({
  name: "partitions",
  initialState,
  reducers: {
    setPartitions: (state, action) => {
      state.pop();
      state.push(action.payload);
    },
  },
});

export const { setPartitions } = partitionsSlice.actions;
export default partitionsSlice.reducer;
