import { createSlice } from "@reduxjs/toolkit";
import { ROW, COLUMN } from "../global/constants";

const initialState = () =>{
  var intervalMatrix = [[], [], [], [], [], [], [], [], [], [], [], [], []];
  for (let i = 0; i <= ROW; i++) {
    for (let j = 0; j <= COLUMN; j++) {
      intervalMatrix[i][j] = "";
    }
  }
  return [intervalMatrix];
} 


export const intervalMatrixSlice = createSlice({
  name: "intervalMatrix",
  initialState,
  reducers: {
    setIntervalMatrix: (state, action) => {
      state.pop();
      state.push(action.payload);
    },
  },
});

export const { setIntervalMatrix } = intervalMatrixSlice.actions;
export default intervalMatrixSlice.reducer;