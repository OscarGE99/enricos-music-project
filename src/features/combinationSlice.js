import { createSlice } from "@reduxjs/toolkit";

const initialState = () => {
  var combination = 0;
  return [combination];
};

export const combinationSlice = createSlice({
  name: "combination",
  initialState,
  reducers: {
    setCombination: (state, action) => {
      state.pop();
      state.push(action.payload);
    },
  },
});

export const { setCombination } = combinationSlice.actions;
export default combinationSlice.reducer;
