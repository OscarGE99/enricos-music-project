import { createSlice } from "@reduxjs/toolkit";
import { ROW, COLUMN } from "../global/constants";

const initialState = () => {
  var preIntervalMatrix = [[], [], [], [], [], [], [], [], [], [], [], [], []];
  for (let i = 0; i <= ROW; i++) {
    for (let j = 0; j <= COLUMN+1; j++) {
      preIntervalMatrix[i][j] = "";
    }
  }
  return [preIntervalMatrix];
};

export const preIntervalMatrixSlice = createSlice({
  name: "preIntervalMatrix",
  initialState,
  reducers: {
    setPreIntervalMatrix: (state, action) => {
      state.pop();
      state.push(action.payload);
    },
  },
});

export const { setPreIntervalMatrix } = preIntervalMatrixSlice.actions;
export default preIntervalMatrixSlice.reducer;
