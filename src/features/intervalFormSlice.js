import { createSlice } from "@reduxjs/toolkit";
import { FORMULA_ARR } from "../global/constants";

const initialState = () => {
  var intervalForm = new Array(FORMULA_ARR).fill("");
  intervalForm[FORMULA_ARR - 1] = "8j";
  return [intervalForm];
};

export const intervalFormSlice = createSlice({
  name: "intervalForm",
  initialState,
  reducers: {
    setIntervalForm: (state, action) => {
      state.pop();
      state.push(action.payload);
    },
  },
});

export const { setIntervalForm } = intervalFormSlice.actions;
export default intervalFormSlice.reducer;
