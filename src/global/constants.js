const VALID_KEYS = [ "z", "x", "c", "v", "b", "n", "m", "s", "d", "g", "h", "j"];
const PRESSEDKEY = new Map([
  ["z", 0],
  ["s", 1],
  ["x", 2],
  ["d", 3],
  ["c", 4],
  ["v", 5],
  ["g", 6],
  ["b", 7],
  ["h", 8],
  ["n", 9],
  ["j", 10],
  ["m", 11]
]);

const INTERVAL_VEC_TH = ["2m", "2M", "3m", "3M", "4j", "4+"];

const ROW = 12;
const COLUMN = 12;
const FORMULA_ARR = 12;
const INTERVAL_VEC = 6;

var reverse = {
  isReverse: false,
};

const INNERDODECAGON = [
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "A",
  "B",
  "0",
  "1",
  "2",
];
const OUTERDODECAGON = [
  "D♯",
  "E",
  "F",
  "F♯",
  "G",
  "G♯",
  "A",
  "A♯",
  "B",
  "C",
  "C♯",
  "D",
];
const NODECOLORS = [
  "#8EFF00",
  "#28A62F",
  "#00FFFB",
  "#006BFF",
  "#A800FF",
  "#FF00E4",
  "#FBFBFB",
  "#9B9B9B",
  "#000000",
  "#FF0000",
  "#FF7B00",
  "#FFD800",
];
const ANGLE = (2 * Math.PI) / 12;
const NODERADIUS = 6;

export {
  VALID_KEYS,
  PRESSEDKEY,
  ROW,
  COLUMN,
  FORMULA_ARR,
  INTERVAL_VEC,
  INTERVAL_VEC_TH,
  reverse,
  INNERDODECAGON,
  OUTERDODECAGON,
  NODECOLORS,
  ANGLE,
  NODERADIUS,
};